CREATE TABLE IF NOT EXISTS `devices` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `imei` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `mac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `requests` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `device_id` int(10) UNSIGNED NOT NULL,
    `request` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
