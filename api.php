<?php

require_once('config.php');

$dsn = $config['db_connection'] . ':host=' . $config['db_host'] . ';dbname=' . $config['db_name'];
$user = $config['db_user'];
$password = $config['db_password'];
$response = '';

if (!empty($_REQUEST['clientName'])) {
    try {
        $pdo = new PDO($dsn, $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $stmt = $pdo->prepare('SELECT id FROM devices WHERE reg_id = :reg_id');
        $stmt->execute(['reg_id' => $_REQUEST['clientName']]);
        $id = $stmt->fetchColumn();
        if ($id) {
            $stmt = $pdo->prepare('SELECT id,request FROM requests WHERE device_id = :id AND status = :status');
            $stmt->execute(['id' => $id, 'status' => 0]);
            $request = $stmt->fetchAll();
            if (!empty($request)) {
                $result = [];
                foreach ($request as $data) {
                    $result[] = $data['request'];
                    $update = $pdo->prepare('UPDATE requests SET status = :status WHERE id = :id');
                    $update->execute(['status' => 2, 'id' => $data['id']]);
                }
                $response = json_encode($result);
                fn_log(count($request) . ' commands have been sent');
            } else {
                fn_log('New command not found for clienName=' . $_REQUEST['clientName']);
                $response = '404';
            }
        } else {
            $stmt = $pdo->prepare('INSERT INTO devices (reg_id) VALUES (:reg_id)');
            $stmt->execute(['reg_id' => $_REQUEST['clientName']]);
            fn_log('clientName ' . $_REQUEST['clientName'] . ' has been registred');
            $response = 'registred';
        }
        echo $response;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function fn_log($content = '')
{
    $text = date("Y-m-d H:i:s") . ' ' . $content . "\n";
    file_put_contents('getCommand.log', $text, FILE_APPEND);
}
